This is clone of spdlog: https://github.com/gabime/spdlog
Version is the latest official release as of 2020-11.

Unnecessary files (examples, tests, benchmarks, ...) have been removed. 

No modification have been made, except adding to CMakeLists.txt:
cmake_policy(SET CMP0077 NEW)  # allow overriding option() value by variables

Some required cmake flasg have been set in parent CMakeLists.txt.

