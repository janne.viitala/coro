# C++20 co-routines

## Introduction

Provides Task<> class template for implementing asynchronous co-routines that can co_await and be co_await'ed. 

Also provides some demo co_await'able async services for co-routines (i.e. Task<>): 
- Timing, for async sleeping
- Threads, for switching between main and background thread

See unit tests for examples.

Includes simple JobRunner class to mimic event loop behavior, allowing application to have "main thread" and execution of async functions in the context of it. 
Also manual memory management, for unit tests to easily check for memory leaks in unit test code - or in real-time system with no dynamic memory allocation.

Unit tests provided, using google test. 

Logging covers pretty much all the operations, making it easier to follow what the code does.


## Compiling and running
It's cmake project, so to build:
```
$ mkdir build
$ cd build
$ cmake ..
$ make 
```

To run unit tests:
```
$ make test
```

To create unit test coverage report - *after* running unit tests:
```
$ make coverage
```
Then open html/index.html in browser.

## Author
Janne Viitala 
[janne.viitala@gmail.com]()

## License
BSD

