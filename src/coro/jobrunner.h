/* JobRunner Singleton provides method to add and remove functions to be executed
 * when JobRunner's run() method is called.
 *
 * Thread safe
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#include <functional>
#include <array>
#include <string>
#include <mutex>

class JobRunner
{
public:
    static const int MAX_JOBS = 100;  // enough for unit tests

    JobRunner ();

    // Return Singleton instance
    static JobRunner *instance ();

    // Add job to execution, return handle >= 0 if ok, <0 if error
    // name is for debugging
    int registerExecution (const std::function<bool(void)>& newJob, const std::string &name);

    // Remove previously registered function from execution.
    void cancelExecution (int handle);

    // Run all registered functions.
    void run ();

    // Return number of jobs currently registered.
    int jobCount ();

private:
    static JobRunner *_instance;
    std::array<std::function<bool(void)>, MAX_JOBS> _jobs;
    std::array<std::string, MAX_JOBS> _names;
    int _jobCount = 0;
    //std::mutex _mutex;
};
