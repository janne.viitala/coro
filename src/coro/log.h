/* Simple wrapper to spdlog logging library (https://github.com/gabime/spdlog)
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#include "spdlog/spdlog.h"

#define AF_LOG_PATTERN "%Y-%m-%d %H:%M:%S.%f %^[%L]%$ %15!s:%-4# {%5!t}: %v"

#define LOG_TRACE(...) SPDLOG_TRACE (__VA_ARGS__)
#define LOG_DEBUG(...) SPDLOG_DEBUG (__VA_ARGS__)
#define LOG_INFO(...) SPDLOG_INFO (__VA_ARGS__)
#define LOG_WARN(...) SPDLOG_WARN (__VA_ARGS__)
#define LOG_ERROR(...) SPDLOG_ERROR (__VA_ARGS__)
#define LOG_CRITICAL(...) SPDLOG_CRITICAL (__VA_ARGS__)

// pretty formatted this
//#define THIS (((reinterpret_cast<unsigned long> (this)) >> 5) & 0xfff)

// human readable'ish id for Task<>
#define TASKID(_task) (((reinterpret_cast<unsigned long> (&_task)) >> 5) & 0xfff)
