/* Static fixed memory pool for co-routines
 *
 * Bunch of blocks, block per co-routine.
 * 10kb ought to be enough for everyone.
 *
 * NOTE: for tracking memory allocation and free'ing in unit tests
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#include <array>
#include <mutex>

class Memory
{
public:
    Memory () = delete;

    // Allocate some memory, like malloc() but from static pool.
    // Returns nullptr if no more memory left in the pool.
    static void *allocBlock (unsigned int bytes);

    // Free previously allocated block
    static void freeBlock (void *);

    // Returns number of blocks currently allocated - handy for tracking memory usage.
    static int usedBlocks ();

private:
    static const int BLOCK_SIZE_B = 1024 * 10;
    static const int BLOCKS = 1000;

    // Extra-rudimentary memory management.
    // Note that returned buffer must be aligned for 8-byte boundary in x86_64
    struct Block
    {
        bool isFree = true;
        alignas(8) char mem[BLOCK_SIZE_B]{};
    };
    static std::array<Block, BLOCKS> _mem;
    static std::mutex _mutex;
};
