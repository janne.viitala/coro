/*  Task
 *
 *  Task class to be used for user defined co-routines.
 *  NOTE: starts eagerly.
 *
 *  Task<int> myCoRoutine (...)
 *  {
 *      ...
 *      co_await ...;
 *      ...
 *      co_return 123;
 *  }
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#include "task_promise.h"

#include "coro/log.h"


/**
 * Task<RETURNTYPE> represents co-routine returning RETURNTYPE value.
 * RETURNTYPE may also be void if there is no value to be returned.
 *
 * @tparam RETURNTYPE is the data type returned by the function, may be void
 */
template<class RETURNTYPE>
class [[nodiscard("non-awaited co-routine lifecycle is tied to returned object")]] Task
{
public :
    using promise_type = NoArgsPromise<Task, RETURNTYPE>;


    explicit Task (std::coroutine_handle<promise_type> handle) noexcept
            : _h (handle)
    {
        LOG_DEBUG ("Task::Task at {}", _h.address ());
        _h.promise().setTask (this);
    }


    /**
     * Creates invalid Task that can be assigned from real one.
     */
    Task () noexcept
    {
        LOG_DEBUG ("Task<> default ctor {}", _h.address ());
    }


    Task (Task &rhs) = delete;


    Task (Task &&rt) noexcept:
            _h (std::move (rt._h)),
            _abortIfDeletingRunningCoroutine (std::move (rt._abortIfDeletingRunningCoroutine))
    {
        LOG_DEBUG ("move constructing {} Task<> {:x} over {} which is {}", rt.finished () ? "finished" : "running", TASKID (rt), _h.address (), finished () ? "finished" : "running");

        rt._h = nullptr;
        LOG_DEBUG ("Task::Taske move c'tor {} from {:x}", _h.address (), TASKID (rt));
    }


    Task &operator= (Task &&rhs) noexcept
    {
        LOG_DEBUG ("moving {} Task<> {:x} over {} which is {}", rhs.finished () ? "finished" : "running", TASKID (rhs), _h.address (), finished () ? "finished" : "running");
        if (this != &rhs)
        {
            if (_h)
            {
                if (!_h.done ())
                {
                    LOG_ERROR ("moving Task over non-finished one!");
                    if (_abortIfDeletingRunningCoroutine)
                    {
                        abort ();
                    }
                }
                LOG_DEBUG ("destroying old coro at {}", _h.address ());
                _h.destroy ();
            }
            _h = std::move (rhs._h);
            rhs._h = nullptr;   // ensure old "empty" handle is not used
            _abortIfDeletingRunningCoroutine = std::move (rhs._abortIfDeletingRunningCoroutine);
        }
        else
        {
            LOG_ERROR ("trying to assing Task to self!");
        }
        return *this;
    }


    ~Task () noexcept
    {
        LOG_DEBUG ("Task d'tor {}", _h.address ());
        if (_h)  // nullptr if this was moved
        {
            if (!_h.done ())
            {
                LOG_CRITICAL ("*** Destroying unfinished coroutine of task {}!", _h.address ());
                if (_abortIfDeletingRunningCoroutine)
                {
                    abort ();
                }
            }
            LOG_DEBUG ("Task d'tor destroying _h {}", _h.address ());
            _h.destroy ();  // FIXME: frees coro ok, but causes "use after free" when coro is calling coro
        }
        else
        {
            LOG_TRACE ("coro of Task {} is already gone, not destroying it", (void *) this);
        }
    }


    [[nodiscard]] bool await_ready () const noexcept
    {
        LOG_DEBUG ("Task::await_ready {}: {}", _h.address (), _h.done () ? "ready" : "NOT ready");
        return (_h.done ());   // if co_await'ing but task is ready already
    }


    bool await_suspend (std::coroutine_handle<> h) noexcept
    {
        LOG_DEBUG ("Task::await_suspend {} in coro {}", h.address (), _h.address ());
        _h.promise ()._caller = h;   // store caller to promise so that we can wake it up when we
        //return false;              // are done (final_suspend()) but we never suspend.
        return true;
    }


    RETURNTYPE await_resume () noexcept
    {
        LOG_DEBUG ("Task<>::await_resume {}", _h.address ());
        if constexpr (!std::is_void_v<RETURNTYPE>)
        {
            return _h.promise ()._value;
        }
        else
        {
            return;
        }
    }


    RETURNTYPE value () const noexcept
    {
        LOG_DEBUG ("Task<>::value() {}", _h.address ());
        // This will not compile for Task<void>. This is intentional, as using value()
        // for void makes absolutely no sense.
        return _h.promise ()._value;
    }


    /**
     * @return true if Task is attached to co-routine
     */
    bool valid ()
    {
        return (_h != nullptr);
    }


    /**
     * @return true if Task is finished.
     */
    bool finished ()
    {
        if (_h)
        {
            return _h.done ();
        }
        return true;
    }


    /**
     * Control if Task should abort() in case it is deleted before co-routine is finished (as
     * this may be considered a serious bug).
     *
     * @param abort true if Task should abort(), false if keep running happily
     */
    void setAbortIfDeletingWhenRunning (bool abort)
    {
        _abortIfDeletingRunningCoroutine = abort;
    }


    /**
     *  Abort the co-routine if it's running
     *  @return true if it was running and was aborted.
     */
    bool abortIfRunning ()
    {
        if (!_h.done ())
        {
            LOG_DEBUG("abortIfRunning(): destroying running co-routine _h {:p}", _h.address ());
            _h.destroy ();
            return true;
        }
        return false;
    }

    void coroDeleted ()
    {
        LOG_DEBUG ("Task coro was deleted!");
        //_h = nullptr;
    }

private:
    std::coroutine_handle<promise_type> _h = nullptr;
    bool _abortIfDeletingRunningCoroutine = false; // true;
};
