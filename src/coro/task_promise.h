/* Base promise_type class for co-routines
 *
 * PromiseBase is base promise_type class for awaitables that returns RETURN_TYPE.
 * Define get_return_object() in your derived promise_type.
 *
 * Set "lazy" to true for lazy starting (suspends initially).
 *
 * See C++20 co-routine specification for documentation.
 *
 * Copyright 2021 Janne Viitala
 */


#pragma once

#include "coro/log.h"
#include "coro/memory.h"

#include <coroutine>
#include <type_traits>

template<typename RETURN_TYPE, bool lazy = false>
class PromiseBase
{
public:

    // Note: function args are constructor args - if no match found then default one is called
    PromiseBase ()
    {
        LOG_DEBUG ("PromiseBase promise_type::promise_type at {}", (void*)this);
    }


    auto initial_suspend ()
    {
        LOG_DEBUG ("PromiseBase {} promise_type::initial_suspend", (void*)this);
        if constexpr (lazy)
        {
            return std::suspend_always{};
        }
        else
        {
            return std::suspend_never{};
        }
    }

    // final_suspend must support "chaining" for coro to call coro succesfully
    auto final_suspend () noexcept
    {
        LOG_DEBUG ("PromiseBase promise_type::final_suspend");
        struct transfer_awaitable
        {
            std::coroutine_handle <> _callingCoro;
            bool await_ready() noexcept
            {
                return false;
            }
            std::coroutine_handle<> await_suspend (std::coroutine_handle<> h) noexcept
            {
                // resume awaiting core if there is one - noop otherwise (e.g. called from norman function)
                return _callingCoro ? _callingCoro : std::noop_coroutine ();
            }
            void await_resume () noexcept {}
        };
        return transfer_awaitable {_caller};
    }

    void unhandled_exception ()
    {
        LOG_DEBUG ("PromiseBase promise_type::unhandled_exception");
        std::abort ();
    }

    /*
     *  promise_type::operator new is used to allocate the whole coroutine frame, so no hidden dynamic
     *  memory allocation here. For this reason @bytes is bigger than sizeof(AWaitable::promise_type),
     *  e.g. 40 bytes in x86_64 gcc 10.1.
     */
    void *operator new (std::size_t bytes)
    {
#ifdef STATIC_MEMORY
        void *addr = Memory::allocBlock (bytes);
#else
        void *addr = malloc (bytes);
#endif
        LOG_DEBUG ("PromiseBase allocating new promise_type of {} bytes at {}", bytes, addr);
        return addr;
    }

    void operator delete (void *ptr) noexcept
    {
        LOG_DEBUG ("PromiseBase deleting promise_type at {}", ptr);
#ifdef STATIC_MEMORY
        Memory::freeBlock (ptr);
#else
        free (ptr);
#endif
    }

    std::coroutine_handle<> _caller;
};

template<typename RETURN_TYPE, bool lazy = false>
class Promise : public PromiseBase<RETURN_TYPE, lazy>
{
public:
    void return_value (RETURN_TYPE& value)
    {
        if constexpr (std::is_arithmetic_v<RETURN_TYPE> || std::is_same_v<RETURN_TYPE, std::string>)
        {
            LOG_DEBUG ("PromiseBase {} promise_type::return_value COPY '{}'", (void*)this, value);
        }
        else
        {
            LOG_DEBUG ("PromiseBase promise_type::return_value COPY of complex type");
        }
        // promise_type may be deleted much earlier than Task, so we need Task to host the
        // return value (if user wants it later)
        _value = (value);
    }

    // moving version of return_value()
    void return_value (RETURN_TYPE &&value)
    {
        if constexpr (std::is_arithmetic_v<RETURN_TYPE> || std::is_same_v<RETURN_TYPE, std::string>)
        {
            LOG_DEBUG ("PromiseBase {} promise_type::return_value MOVE '{}'", (void*)this, value);
        }
        else
        {
            LOG_DEBUG ("PromiseBase promise_type::return_value MOVE of complex type");
        }
        _value = std::move (value);
    }

    RETURN_TYPE _value;
};


template<>
class Promise<void> : public PromiseBase<void, false>
{
public:
    void return_void ()
    {
        LOG_DEBUG ("PromiseBase {} return_void", (void*)this);
    }
};


/*
 *  NoArgsPromise - promise_type for co-routine classes that take no arguments
 */
template<typename TASK, typename RETURN_TYPE>
class NoArgsPromise : public Promise<RETURN_TYPE>
{
public:
    TASK get_return_object ()
    {
        LOG_DEBUG ("NoArgsPromise promise_type::get_return_object");
        return TASK {std::coroutine_handle<NoArgsPromise>::from_promise (*this)};
    }

    ~NoArgsPromise ()
    {
        LOG_DEBUG("NoArgsPromise deleted");
        if (_task)
        {
            _task->coroDeleted ();
        }
    }

    void setTask (TASK *t)
    {
        _task = t;
    }

private:
    TASK* _task = nullptr;

};
