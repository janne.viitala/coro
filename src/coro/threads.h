/* Testing service for async library - switching between main and background thread
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#include "coro/log.h"
#include "coro/jobrunner.h"   // main thread execution context provided by JobRunner

#include <thread>
#include <coroutine>

class AsyncBackgroundThread
{
public:

    /**
     * @brief Switches execution to newly created background thread
     *
     * @return awaitable object, caller MUST co_await this or application WILL malfunction
     */
    [[nodiscard("You must co_await value returned by this function")]]
    auto switchToBackground ()
    {
        class Awaiter
        {
        public:
            explicit Awaiter (AsyncBackgroundThread *parent) : m_thread { parent->m_thread}
            {
                LOG_TRACE ("switchToBackground Awaiter created");
            }

            ~Awaiter ()
            {
                LOG_TRACE ("switchToBackground Awaiter deleted");
            }

            bool await_ready ()
            {
                LOG_TRACE ("switchToBackground::await_ready() -> false");
                return true; //false;  // FIXME TEST
            }

            void await_resume ()
            {
                LOG_TRACE ("switchToBackground::await_resume()");
            }

            void await_suspend (std::coroutine_handle<> h_caller)
            {
                LOG_TRACE ("switchToBackground::switchToBackground()");
                m_thread = std::thread ([h_caller] () {
                    LOG_TRACE ("switchToBackground::switchToBackground lambda resuming caller {} in bgnd thread", h_caller.address ());
                    h_caller.resume ();
                });
                m_thread.detach ();
                LOG_TRACE ("switchToBackground::switchToBackground() DONE");
            }

        private:
            std::thread &m_thread;
        };

        return Awaiter {this};
    }

    /**
     * @brief Switches execution back to main thread from background thread.
     *
     * @return awaitable object, caller MUST co_await this or application WILL malfunction
     */
    [[nodiscard("You must co_await value returned by this function")]]
    auto switchToMain ()
    {
        class Awaiter
        {
        public:
            explicit Awaiter (AsyncBackgroundThread *parent) : m_thread { parent->m_thread}
            {
                LOG_TRACE ("switchToMain Awaiter created");
            }

            ~Awaiter ()
            {
                LOG_TRACE ("switchToMain Awaiter deleted");
            }

            bool await_ready ()
            {
                LOG_TRACE ("switchToMain::await_ready() -> false");
                return false;
            }

            void await_resume ()
            {
                LOG_TRACE ("switchToMain::await_resume()");
                m_suspended = false;
            }

            bool await_suspend (std::coroutine_handle<> h_caller)
            {
                m_suspended = true;
                LOG_TRACE ("switchToMain::await_suspend()");
                JobRunner::instance ()->registerExecution ([h_caller, this] () {
                    LOG_TRACE ("switchToMain::await_suspend lambda: coro is {}, suspended: {}", h_caller ? "ok" : "bad", m_suspended ? "yes" : "NOT suspended");
                    LOG_TRACE ("switchToMain::await_suspend lambda: resuming caller {}, which is {}", h_caller.address (), h_caller.done () ? "done" : "running");
                    h_caller.resume ();
                    return true;
                }, "switchToMain");
                LOG_TRACE ("switchToMain::await_suspend() DONE");
                return true;
            }

        private:
            std::thread &m_thread;
            bool m_suspended = false;
        };

        return Awaiter {this};
    }
private:
    std::thread m_thread;
};
