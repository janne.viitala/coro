/* Testing service for async library - async sleeping.
 *
 * NOTE: uses virtual time to speed up running of unit test.
 *
 * Copyright 2021 Janne Viitala
 */

#pragma once

#include "coro/log.h"
#include "coro/jobrunner.h"

#include <coroutine>

class Timing
{
public:

    static const int CYCLE_TIME_ms = 10;  // FIXME: unit test depend on this but don't use this constant

    // Advance clock with fixed amount (CYCLE_TIME_ms)
    static void cycle ();

    // Return the current virtual monotonic time.
    static int time_ms ();

    /*
     * Sleep asynchronously, with eager starting (sleep starts from call to this function)
     * @param t_ms time to sleep [ms]
     * @param lazy true is time starts from co_await, false means time start from call to the function
     * @return awaiter object that can be co_await'ed
     */
    [[nodiscard]]
    static auto sleepAsync (int t_ms, bool lazy = false)
    {
        struct Awaiter
        {
            Awaiter (int t_ms, bool lazy) :
                    _sleep_ms{t_ms},
                    _lazy{lazy}
            {
                _startTime_ms = Timing::time_ms ();
                LOG_TRACE ("sleepAsync c'tor for {} ms, startTime={}", t_ms, _startTime_ms);
            };

            ~Awaiter ()
            {
                LOG_TRACE ("sleepAsync for {} ms d'tor", _sleep_ms);
                if (_job >= 0)
                {
                    LOG_TRACE ("sleepAsync: job still running, cancelling it");
                    JobRunner::instance ()->cancelExecution (_job);
                }
            }

            bool await_ready ()
            {
                _endTime_ms = _startTime_ms + _sleep_ms;
                _finished = (Timing::time_ms () >= (_endTime_ms));
                LOG_TRACE ("sleepAsync::await_ready {}: {} (time={}, endTime={})", (void*)this, _finished ? "true" : "false",
                           Timing::time_ms (), _startTime_ms + _sleep_ms);
                return _finished;
            }


            void await_resume ()
            {
                LOG_TRACE ("sleepAsync::await_resume()");
            }


            bool await_suspend (std::coroutine_handle<> h_caller)
            {
                if (_lazy)
                {
                    _startTime_ms = Timing::time_ms ();  // if lazy start time from now instead from c'tor
                    _endTime_ms = _startTime_ms + _sleep_ms;
                    LOG_DEBUG ("starting lazy delay of {}ms at {}, ending {}", _sleep_ms, _startTime_ms, _endTime_ms);
                }
                // Note: take a copy of h_caller, as it's just a non-owning reference to the co-routine
                _job = JobRunner::instance ()->registerExecution ([this, h_caller] () {
                    LOG_DEBUG ("sleepAsync lambda: endtime={}, clock={}", _endTime_ms, Timing::time_ms ());
                    if (Timing::time_ms () >= _endTime_ms)
                    {
                        _job = -1;
                        LOG_DEBUG ("TO {} delay of {} ms passed",  (void*)this, _sleep_ms);
                        _finished = true;
                        h_caller.resume ();
                        return true;
                    }
                    return false;
                }, "sleepAsync");
                LOG_DEBUG ("sleepAsync {} is now suspended, wait job {}",  (void*)this, _job);
                return true;  // true = suspends
            }


            // Our all Awaiters provide this
            bool finished ()
            {
                return _finished;
            }

            // data - just POD, so default copy is fine
            int _sleep_ms, _endTime_ms = 0, _startTime_ms = 0;
            int _job = -1;
            bool _finished = false;
            bool _lazy;
        };

        return Awaiter (t_ms, lazy);
    }

    // Sleep asynchronously, with lazy starting (wait starts from co_await)
    [[nodiscard]]
    static auto sleepLazyAsync (int t_ms)
    {
        return sleepAsync (t_ms, true);
    }

};


