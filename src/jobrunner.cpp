/*
 * Copyright 2021 Janne Viitala
 */

#include "coro/jobrunner.h"
#include "coro/log.h"

/* static */ JobRunner *JobRunner::_instance = nullptr;

namespace
{
std::mutex g_createMutex;
}

JobRunner::JobRunner ()
{
    _jobs.fill (nullptr);
}


JobRunner *JobRunner::instance ()
{
    std::lock_guard<std::mutex> lock (g_createMutex);
    if (!_instance)
    {
        _instance = new JobRunner;
    }
    return _instance;
}


int JobRunner::registerExecution (const std::function<bool ()> &newTask, const std::string &name)
{
//    LOG_TRACE("LOCK1 - registerExecution");
//    std::lock_guard<std::mutex> lock (_mutex);
//    LOG_TRACE("LOCK1 gotten");
    for (int ix=0; ix<MAX_JOBS; ++ix)
    {
        if (!_jobs[ix])
        {
            LOG_TRACE ("+ job {} '{}' added", ix, name);
            _jobs[ix] = newTask;
            _names[ix] = name;
            _jobCount ++;
            return ix;
        }
    }
    LOG_CRITICAL ("no more space left in task buffer!");
    return false;
}


void JobRunner::cancelExecution (int handle)
{
//    LOG_TRACE("LOCK2 - cancelExecution");
//    std::lock_guard<std::mutex> lock (_mutex);
//    LOG_TRACE("LOCK2 gotten");
    if (_jobs[handle])
    {
        _jobs[handle] = nullptr;
        _jobCount --;
        LOG_TRACE ("- job {} '{}' cancelled", handle, _names[handle]);
    }
}


void JobRunner::run ()
{
//    LOG_TRACE("LOCK3 - run - there are {} jobs", _jobCount);
//    std::lock_guard<std::mutex> lock (_mutex);
//    LOG_TRACE("LOCK3 gotten");
    for (int ix=0; ix<MAX_JOBS; ++ix)
    {
        if (_jobs[ix])
        {
            LOG_TRACE ("run job {} '{}'", ix, _names[ix]);
            if (_jobs[ix] ())
            {
                if (_jobs[ix])  // job may have cancelled itsef in it's last execution
                {
                    _jobs[ix] = nullptr;
                    _jobCount --;
                }
                LOG_TRACE ("- job {} '{}' completed", ix, _names[ix]);
            }
        }
    }
}


int JobRunner::jobCount ()
{
    return _jobCount;
}


