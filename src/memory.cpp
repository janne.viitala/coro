/*
 * Copyright 2021 Janne Viitala
 */

#include "coro/memory.h"
#include "coro/log.h"

#include <cstring>

/* static */
const int Memory::BLOCK_SIZE_B;
const int Memory::BLOCKS;
std::array<Memory::Block, Memory::BLOCKS> Memory::_mem;
std::mutex Memory::_mutex;


/* static */
void *Memory::allocBlock (unsigned int bytes)
{
    std::lock_guard<std::mutex> lock (_mutex);
    if (bytes > BLOCK_SIZE_B)
    {
        LOG_CRITICAL ("FATAL: requesting too large co-routine frame of {} bytes! Max is {} bytes", bytes, BLOCK_SIZE_B);
        abort ();
    }
    for (int ix = 0; ix < BLOCKS; ++ix)
    {
        if (_mem[ix].isFree)
        {
            LOG_TRACE ("allocated block {}", ix);
            _mem[ix].isFree = false;
            std::memset (&(_mem[ix].mem), 0xff, BLOCK_SIZE_B);
            return &(_mem[ix].mem[0]);
        }
    }
    LOG_CRITICAL ("FATAL: no more free memory blocks for co-routine frames");
    abort ();
}


void Memory::freeBlock (void *ptr)
{
    std::lock_guard<std::mutex> lock (_mutex);
    for (int ix = 0; ix < BLOCKS; ++ix)
    {
        void *mp = &(_mem[ix].mem);
        if (ptr == mp)
        {
            _mem[ix].isFree = true;   /// !!!!! ?????? bugaa jos tää mukana - miksi ???
            std::memset (&(_mem[ix].mem), 0xff, BLOCK_SIZE_B);
            LOG_TRACE ("free'd block {}, now {} blocks used", ix, usedBlocks ());
            return;
        }
    }
    LOG_CRITICAL ("FATAL: failed to free memory block from {}", ptr);
    abort ();
}


int Memory::usedBlocks ()
{
    int used = 0;
    for (int ix = 0; ix<BLOCKS; ++ix)
    {
        if (!_mem[ix].isFree)
        {
            used ++;
            //LOG_TRACE("usedBlocks: block {} is used", ix);
        }
    }
    return used;
}

