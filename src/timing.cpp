/*
 * Copyright 2021 Janne Viitala
 */

#include "coro/timing.h"

namespace
{
static int t_ms;
}


void Timing::cycle ()
{
    t_ms += CYCLE_TIME_ms;
}


int Timing::time_ms ()
{
    return t_ms;
}
