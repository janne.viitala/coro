/*
 * co-routine library unit tests.
 *
 * Copyright 2021 Janne Viitala
 */

#include "coro/task.h"
#include "coro/timing.h"
#include "coro/threads.h"

#include <gtest/gtest.h>

// Macros to toggle logging, to focus on interesting stuff
#define LOG_ON  spdlog::set_level (spdlog::level::trace)
#define LOG_OFF spdlog::set_level (spdlog::level::critical)

#ifdef STATIC_MEMORY
/*
 * first test: all memory blocks free
 */
TEST (coro, initialmemok)
{
    EXPECT_EQ(Memory::usedBlocks(), 0);
}
#endif

// helper to run "event" loops for a while
static void loop_10ms (int loops)
{
    int cnt = loops;
    LOG_DEBUG ("entering LOOP of {} ms", loops * 10);
    while (cnt > 0)
    {
        cnt--;
        Timing::cycle ();
        LOG_TRACE ("cycle {}/{}", loops - cnt, loops);
        JobRunner::instance ()->run ();
    }
    LOG_DEBUG ("LOOP of {} ms done", loops * 10);
}

Task<void> simplestVoidCoro ()
{
    LOG_TRACE ("simplestVoidCoro returning");
    co_return;
}

Task<void> simpleVoidCaller ()
{
    LOG_TRACE ("simpleCaller calls simplestCoro => variable");
    auto t = simplestVoidCoro ();
    EXPECT_EQ (t.finished (), true) << "coro finished synchronously";
    LOG_TRACE ("simpleCaller awaits simplestCoro variable");
    co_await t;
    LOG_TRACE ("simpleCaller awaits simplestCoro call");
    co_await simplestVoidCoro ();
    EXPECT_EQ (t.finished (), true) << "coro finished synchronously";
    LOG_TRACE ("simpleCaller returning");
}

static int i_ret = 0;

Task<int> simplestIntCoro ()
{
    LOG_TRACE ("simplestCoro returning {}", i_ret);
    co_return i_ret;
}

Task<int> simplestIntCoroCaller ()
{
    int i = co_await simplestIntCoro ();
    LOG_TRACE ("simplestIntCoroCaller returning {}", i);
    co_return i;
}

// sync void coro
TEST (coro, simplestVoidSynchronous)
{
    auto t1 = simplestVoidCoro ();
    EXPECT_EQ (t1.finished (), true) << "coro finished synchronously";
    //auto t2 = simpleVoidCaller ();
    //EXPECT_EQ (t2.finished (), true) << "coro finished synchronously";
    // t2.value () - does not exist
}

// sync int coro
TEST (coro, simplestIntSynchronous)
{
    i_ret = 33;
    auto t1 = simplestIntCoro ();
    EXPECT_EQ (t1.finished (), true) << "coro finished synchronously";
    EXPECT_EQ (t1.value (), 33);
    i_ret = 39;
    auto t2 = simplestIntCoroCaller ();
    EXPECT_EQ (t2.finished (), true) << "coro finished synchronously";
    EXPECT_EQ (t2.value (), 39);
}

Task<void> testAsyncSleep_ms (int ms)
{
    auto t = Timing::sleepAsync (ms);
    EXPECT_FALSE(t.finished ()) << "timeout just created and not finished";
    LOG_DEBUG ("co_awaiting sleepAsync...");
    co_await t;
    LOG_DEBUG ("co_awaiting sleepAsync DONE!");
    EXPECT_TRUE(t.finished ()) << "timeout now finished";
    co_return;
}

Task<int> testAsyncSleep2_ms (int ms, bool skip_await = false)
{
    auto t = Timing::sleepAsync (ms);
    EXPECT_FALSE(t.finished ()) << "timeout just created and not finished";
    LOG_DEBUG ("co_awaiting sleepAsync...");
    if (!skip_await)
    {
        co_await t;
    }
    LOG_DEBUG ("co_awaiting sleepAsync DONE!");
    if (skip_await && ms != 0)
    {
        EXPECT_FALSE(t.finished ()) << "timeout NOT finished";
    }
    else
    {
        EXPECT_TRUE(t.finished ()) << "timeout now finished";
    }
    co_return 123;
}


TEST (coro, simplestAsyncCall)
{
    auto t = testAsyncSleep_ms (20);
    EXPECT_FALSE(t.finished ()) << "timeout not finished";
    loop_10ms (3);
    EXPECT_TRUE(t.finished ()) << "timeout not finished";
}

TEST (coro, timeout_simple)
{
    auto t = testAsyncSleep_ms (100);
    EXPECT_FALSE(t.finished ()) << "timeout not finished";
    loop_10ms (7);
    EXPECT_FALSE(t.finished ()) << "timeout not just yet finished";
    loop_10ms (5);
    EXPECT_TRUE(t.finished ()) << "timeout finished";
    LOG_DEBUG("---------------");
    t = testAsyncSleep_ms (100);
    EXPECT_FALSE(t.finished ()) << "timeout not finished";
    loop_10ms (7);
    EXPECT_FALSE(t.finished ()) << "timeout not just yet finished";
    loop_10ms (5);
    EXPECT_TRUE(t.finished ()) << "timeout finished";
}


TEST (coro, timout)
{
    auto t = testAsyncSleep_ms (100);
    EXPECT_FALSE(t.finished ()) << "timeout not finished";
    loop_10ms (7);
    EXPECT_FALSE(t.finished ()) << "timeout not just yet finished";
    loop_10ms (5);
    EXPECT_TRUE(t.finished ()) << "timeout finished";

    auto t2 = testAsyncSleep2_ms (50);
    EXPECT_FALSE(t2.finished ());
    loop_10ms (6);
    EXPECT_TRUE(t2.finished ());
    EXPECT_EQ (t2.value (), 123);

    // now skip await - Timeout shall be deleted before it's finished
    auto t3 = testAsyncSleep2_ms (50, true);
    EXPECT_TRUE(t3.finished ()) << "was finished synchronously";
    loop_10ms (6);

    // synchronous (0ms) delay
    auto t4 = testAsyncSleep2_ms (0);
    EXPECT_TRUE(t4.finished ()) << "was finished synchronously";

    // test re-use of e..g t4 !!
    t4 = testAsyncSleep2_ms (0);
    EXPECT_TRUE(t4.finished ()) << "was finished synchronously";

    t4 = testAsyncSleep2_ms (50);
    EXPECT_FALSE(t4.finished ()) << "was not finished synchronously";
    loop_10ms (6);
    EXPECT_TRUE(t4.finished ()) << "was not finished synchronously";

    // re-use void task
    t = testAsyncSleep_ms (30);
    LOG_TRACE ("Task {:x} is {}", TASKID (t), t.finished ());
    EXPECT_FALSE(t.finished ()) << "was not finished synchronously";
    loop_10ms (5);
    EXPECT_TRUE(t.finished ()) << "was not finished synchronously";

    LOG_DEBUG("Test ending, deleting stuff");
}

Task<void> loopper (int cnt)
{
    for (int i = 0; i < cnt; ++i)
    {
        co_await testAsyncSleep_ms (0);  // completes synchronously
    }
}

TEST (coro, syncloop)
{
    auto t = loopper (10);  // this has been up 1M
    EXPECT_TRUE(t.finished ());
}

struct NonPOD
{
    int v;
    std::string s;
    double d;

    NonPOD () = default;

    NonPOD (NonPOD &rhs)
    {
        v = rhs.v;
        s = rhs.s;
        d = rhs.d;
    }

    NonPOD (NonPOD &&rhs)
    {
        v = rhs.v;
        s = std::move (rhs.s);
        d = rhs.d;
    }

    NonPOD &operator= (NonPOD &&rhs)
    {
        v = rhs.v;
        s = std::move (rhs.s);
        d = rhs.d;
        return *this;
    }

    NonPOD &operator= (NonPOD &rhs)
    {
        v = rhs.v;
        s = rhs.s;
        d = rhs.d;
        return *this;
    }

};

Task<NonPOD> task (int ms)
{
    NonPOD obj;
    obj.d = 12.3;
    co_await Timing::sleepAsync (ms);
    obj.v = ms;
    obj.s = "hehee";
    co_return obj;
};

TEST (coro, nonPOD)
{
    const int DELAY_ms = 30;
    auto t = task (DELAY_ms);
    EXPECT_FALSE(t.finished ());
    loop_10ms (4);
    EXPECT_TRUE(t.finished ());
    auto obj = t.value ();
    EXPECT_EQ(obj.v, DELAY_ms);
    EXPECT_EQ(obj.d, 12.3);
    EXPECT_EQ(obj.s, "hehee");

    t = task (0);
    EXPECT_TRUE(t.finished ());
    obj = t.value ();
    EXPECT_EQ(obj.v, 0);
}

// with eager starting 30+20 = 30
Task<void> eagerTest ()
{
    auto t1 = Timing::sleepAsync (20);
    auto t2 = Timing::sleepAsync (30);
    LOG_TRACE ("    >>> eagerTest start 1st await");
    co_await t1;
    LOG_TRACE ("    >>> eagerTest start 2nd await");
    co_await t2;
    LOG_TRACE ("    >>> eagerTest returning");
}

// with lazy starting 30+20 = 50
Task<void> lazyTest ()
{
    auto t1 = Timing::sleepLazyAsync (20);
    auto t2 = Timing::sleepLazyAsync (30);
    LOG_TRACE (">>> lazyTest start 1st await");
    co_await t1;
    LOG_TRACE (">>> lazyTest start 2nd await");
    co_await t2;
    LOG_TRACE (">>> lazyTest returning");
}

TEST (coro, eagerDelay)
{
    auto t = eagerTest ();
    EXPECT_FALSE(t.finished ());
    loop_10ms (4);
    EXPECT_TRUE(t.finished ());
}


TEST (coro, lazydelay)
{
    auto t = lazyTest ();
    EXPECT_FALSE(t.finished ());
    loop_10ms (4);
    EXPECT_FALSE(t.finished ());  // not yet, it takes 30+20=50ms
    loop_10ms (3);
    EXPECT_TRUE(t.finished ());
}

TEST (coro, copyAndMove)
{
    Task<void> t;
    EXPECT_FALSE(t.valid ());
    EXPECT_TRUE(t.finished ());
    t = lazyTest ();
    EXPECT_TRUE(t.valid ());
    //auto t2 = t;   does not compile - can't copy Tasks
    LOG_TRACE ("next moving Task");
    auto t2 = std::move (t);
    EXPECT_TRUE(t2.valid ());
    EXPECT_FALSE(t.valid ());

    LOG_TRACE ("moved Task!");
    EXPECT_FALSE (t2.finished ());

    // this silliness should not corrupt anything
    t2 = std::move (t2);
    EXPECT_TRUE(t2.valid ());
    EXPECT_FALSE (t2.finished ());

    loop_10ms (7);
    EXPECT_TRUE (t2.finished ());
}

// with eager starting 30+20 = 30
Task<void> calledTask ()
{
    LOG_TRACE ("   >>> calledTask awaiting sleepAsync");
    co_await Timing::sleepAsync (20);
    LOG_TRACE ("   >>> calledTask returning");
    co_return;
}

Task<int> callerTask ()
{
    LOG_TRACE (" >> callerTask awaiting calledTask");
    co_await calledTask ();
    LOG_TRACE (" >> callerTask returning");
    co_return 1234;
}

TEST (coro, taskCallingTaskOnce)
{
    auto t = callerTask ();
    EXPECT_FALSE(t.finished ());
    loop_10ms (3);
    EXPECT_TRUE(t.finished ());
    LOG_INFO ("taskCallingTask test completed");
}


TEST (coro, taskCallingTaskTwice)
{
    auto t = callerTask ();
    EXPECT_FALSE(t.finished ());
    loop_10ms (3);
    EXPECT_TRUE(t.finished ());
    LOG_INFO ("taskCallingTask test completed");
    EXPECT_EQ(t.value (), 1234);
    LOG_DEBUG("------------------------------------------");
    t = callerTask ();
    EXPECT_FALSE(t.finished ());
    loop_10ms (3);
    EXPECT_TRUE(t.finished ());
    LOG_INFO ("taskCallingTask test completed");
    EXPECT_EQ(t.value (), 1234);
}

Task<int> twiceCallerTask ()
{
    LOG_TRACE (" >> twiceCallerTask awaiting calledTask");
    co_await calledTask ();
    LOG_TRACE (" >> twiceCallerTask 1st pass done!");
    LOG_DEBUG("------------------------------------------");
    LOG_TRACE (" >> twiceCallerTask awaiting calledTask AGAIN");
    co_await calledTask ();
    LOG_TRACE (" >> twiceCallerTask returning");
    co_return 1234;
}


TEST (coro, taskCallingTaskTwiceCallerOnce)
{
    auto t = twiceCallerTask ();
    EXPECT_FALSE(t.finished ());
    // NOTE: need to have 4 here, as 1st time is called during JobRunner::run() due to test
    //       implementation of it (that uses simulated time)
    loop_10ms (4);
    EXPECT_TRUE(t.finished ());
    LOG_INFO ("taskCallingTask test completed");
}



// run some timeouts in parallel
TEST (coro, vectorOfLazyTasks)
{
    std::vector<Task<void>> vec;

    const int TASKS = 10;

    for (int i = 0; i < TASKS; ++i)
    {
        vec.push_back (lazyTest ());
    }
    EXPECT_EQ(JobRunner::instance ()->jobCount (), TASKS);

    loop_10ms (4);
    for (auto &t: vec)
    {
        EXPECT_FALSE (t.finished ());
    }
    loop_10ms (2);
    for (auto &t: vec)
    {
        EXPECT_TRUE (t.finished ());
    }
}

Task<void> simpleThreadTest ()
{
    AsyncBackgroundThread thr;
    auto t = std::this_thread::get_id ();
    LOG_TRACE ("  >>> still in main thread");
    //co_await thr.switchToBackground ();
  //    EXPECT_NE (std::this_thread::get_id (), t);

    /*
     *  Bugi: taustasäie ehtii Thread::switchToMain():iin ja postaa siellä job:in ENNENKUIN
     *  edellinen switchToMain job on completed. Tää ei vissiin vielä sinänsä haittaa, mutta
     *  taustasäie ehtii tehdä
     *    co_await Threads::switchToMain ();
     *  ENNENKUIN pääsäie on saanyt aiemman
     *    co_await Threads::switchToBackground ();
     *  valmiiksi. Bugaava case:
     *
2022-05-12 13:04:12.820088 [T]   coro_test.cpp:386  {31683}:   >>> still in main thread
2022-05-12 13:04:12.820097 [T]       threads.h:30   {31683}: switchToBackground::await_ready() -> false
2022-05-12 13:04:12.820106 [T]       threads.h:41   {31683}: switchToBackground::switchToBackground()
2022-05-12 13:04:12.820250 [T]       threads.h:47   {31683}: switchToBackground::switchToBackground() DONE
2022-05-12 13:04:12.820264 [T]       threads.h:43   {31685}: switchToBackground::switchToBackground lambda resuming caller 0x7fea04212d70 in bgnd thread
2022-05-12 13:04:12.820275 [D]          task.h:112  {31683}: TaskBase::await_ready 82d: NOT ready
2022-05-12 13:04:12.820302 [T]       threads.h:36   {31685}: switchToBackground::await_resume()
2022-05-12 13:04:12.820319 [T]   coro_test.cpp:402  {31685}:   >>> now in bg thread
2022-05-12 13:04:12.820330 [T]       threads.h:66   {31685}: switchToMain::await_ready() -> false   <<<< tässä taustasäie ajaa switchToMain()
2022-05-12 13:04:12.820343 [T]       threads.h:77   {31685}: switchToMain::await_suspend()
2022-05-12 13:04:12.820361 [T]   jobrunner.cpp:41   {31685}: + job 1 'switchToMain' added
2022-05-12 13:04:12.820375 [T]       threads.h:83   {31685}: switchToMain::await_suspend() DONE
2022-05-12 13:04:12.820445 [D]          task.h:119  {31683}: TaskBase::await_suspend 82d            <<<< tässä pääsäien vasta suspendoi coron switchToBackground() jälkeen
2022-05-12 13:04:12.820465 [T]   jobrunner.cpp:84   {31683}: - job 0 'switchToMain' completed
2022-05-12 13:04:12.820478 [T]   jobrunner.cpp:76   {31683}: run job 1 'switchToMain'
2022-05-12 13:04:12.820491 [T]       threads.h:79   {31683}: switchToMain::await_suspend lambda resuming caller 0x7fea04212d70  <<<< lambda resumoi
2022-05-12 13:04:12.820504 [D]  task_promise.h:87   {31683}: PromiseBase deleting promise_type at 0x7fea04212d70                <<<< PÄÄSÄIE tuhoaa promisen
     *
     * Ilmeisesti tämä sekoittaa coro:n tilan käsittelyn tykkänään ja se sekoaa ?? Tai sitten ei - kts.alla:
     *
     * Toinen tapa failata:
     *
2022-05-12 13:50:09.219021 [T]   coro_test.cpp:387  {37549}:   >>> still in main thread
2022-05-12 13:50:09.219031 [T]       threads.h:31   {37549}: switchToBackground Awaiter created
2022-05-12 13:50:09.219041 [T]       threads.h:41   {37549}: switchToBackground::await_ready() -> false
2022-05-12 13:50:09.219051 [T]       threads.h:52   {37549}: switchToBackground::switchToBackground()
2022-05-12 13:50:09.219238 [T]       threads.h:58   {37549}: switchToBackground::switchToBackground() DONE
2022-05-12 13:50:09.219252 [T]       threads.h:54   {37552}: switchToBackground::switchToBackground lambda resuming caller 0x7fa9da78ed70 in bgnd thread
2022-05-12 13:50:09.219262 [D]          task.h:112  {37549}: TaskBase::await_ready 62d: NOT ready
2022-05-12 13:50:09.219278 [D]          task.h:119  {37549}: TaskBase::await_suspend 0x7fa9da78c568
2022-05-12 13:50:09.219295 [T]   jobrunner.cpp:84   {37549}: - job 0 'switchToMain' completed
2022-05-12 13:50:09.219311 [D]   coro_test.cpp:39   {37549}: LOOP of 10 ms done
2022-05-12 13:50:09.219409 [D]  task_promise.h:87   {37552}: PromiseBase deleting promise_type at 0x7fa9da78ed70   <<< tässä TAUSTASÄIE tuhoaa promisen!
     *
     */
    //std::this_thread::sleep_for (std::chrono::milliseconds (100));

    LOG_TRACE ("  >>> now in bg thread");
    co_await thr.switchToMain ();
    LOG_TRACE ("  >>> back in main thread again");
    EXPECT_EQ (std::this_thread::get_id (), t);
    // BUG: here simpleThreadTest Task is deleted, deleting also currenly running switchToMain Task ?!
    co_return;
}


TEST (coro, thread)
{
    auto t = simpleThreadTest ();
    /*
     * Here we can't just loop_10ms() enough times, as test function is running a separate
     * thread. Yet we need to call loop_10ms() as it run()'s the JobRunner. As we now have
     * no means to join() the background thread we just wait 10ms (in real world time) and
     * run "virtual_time" loop_10ms() couple of times and hope thread to finish within
     * this time.
     *
     * In real application we would have some way to join() the background thread instead.
     *
     */
    EXPECT_FALSE (t.finished());
    for (int i=0; i<5; ++i)
    {
        loop_10ms (1); // run some loops to provide execution context
        std::this_thread::sleep_for (std::chrono::milliseconds (10));
    }
    EXPECT_TRUE (t.finished());
}

// call Task that uses Threads::switchToBackground ();
Task<void> bgTaskCaller ()
{
    LOG_DEBUG ("  ***>>> call bgdn task - 1st try");
    co_await simpleThreadTest ();
    LOG_DEBUG ("  ***>>> call bgdn task - 2nd try");
    co_await simpleThreadTest ();
    LOG_DEBUG ("  ***>>> call bgdn task - 2nd try DONE");
    co_return;
}

TEST (coro, thread2)
{
    LOG_ON;
    auto t = bgTaskCaller ();
    EXPECT_FALSE (t.finished());
    for (int i=0; i<3; ++i)
    {
        loop_10ms (1); // run some loops to provide execution conte
        std::this_thread::sleep_for (std::chrono::milliseconds (10));
    }
    EXPECT_TRUE (t.finished());
}


Task<void> timeoutOverwriter ()
{
    auto t = Timing::sleepAsync (50);
    t = Timing::sleepAsync (20);
    co_await t;
}

TEST (coro, overwriteTimeout)
{
    LOG_ON;
    auto t = timeoutOverwriter ();
    loop_10ms (10);
}


#ifdef STATIC_MEMORY
/*
 * final test: all memory blocks free'd?
 * Use valgrind of not STATIC_MEMORY
 */
TEST (coro, memok)
{
    EXPECT_EQ(Memory::usedBlocks(), 0);
}
#endif


int main (int argc, char *argv[])
{
    spdlog::set_pattern (AF_LOG_PATTERN);
    LOG_ON; //LOG_OFF;
    testing::InitGoogleTest (&argc, argv);
    return RUN_ALL_TESTS ();
}
